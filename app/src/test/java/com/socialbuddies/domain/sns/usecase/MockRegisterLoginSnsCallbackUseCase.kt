package com.socialbuddies.domain.sns.usecase

import android.view.View

/**
 * Created 11/05/17
 */
class MockRegisterLoginSnsCallbackUseCase : RegisterLoginSnsCallbackUseCase {
    var callback: RegisterLoginSnsCallbackUseCase.LoginCallback? = null

    override fun registerLoginCallback(loginView: View, loginCallback: RegisterLoginSnsCallbackUseCase.LoginCallback) {
        callback = loginCallback
    }

    fun callSuccess() {
        callback?.onSuccess()
    }

    fun callError() {
        callback?.onError()
    }

}