package com.socialbuddies.ui.search

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.reset
import com.nhaarman.mockito_kotlin.verify
import com.socialbuddies.domain.network.usecase.CheckNetworkConnectionUseCase
import com.socialbuddies.domain.sns.model.Buddy
import com.socialbuddies.domain.sns.usecase.GetBuddiesUseCase
import com.socialbuddies.domain.sns.usecase.GetUsernameUseCase
import com.socialbuddies.domain.sns.usecase.LogoutSnsUseCase
import com.socialbuddies.domain.stub.StubBuddiesManager
import com.socialbuddies.test.ImmediateSchedulerInitializer
import com.socialbuddies.ui.search.presenter.SearchPresenterImpl
import com.socialbuddies.ui.search.presenter.contract.SearchView
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

/**
 * Created 11/05/17
 */
class SearchPresenterImplTest {

    val USERNAME = "User"

    lateinit var presenter: SearchPresenterImpl

    @Mock lateinit var mockLogoutUseCase: LogoutSnsUseCase
    @Mock lateinit var mockView: SearchView
    var mockGetBuddiesUseCase = StubBuddiesManager()
    @Mock lateinit var mockCheckConnectionUseCase: CheckNetworkConnectionUseCase
    @Mock lateinit var mockUsernameUseCase: GetUsernameUseCase

    @Before
    fun initMocks() {
        ImmediateSchedulerInitializer.initImmediateScheduler()
        MockitoAnnotations.initMocks(this)

        `when`(mockLogoutUseCase.logout()).thenReturn(Single.fromCallable{Any()})
        `when`(mockCheckConnectionUseCase.isOnline()).thenReturn(Single.fromCallable{true})
        `when`(mockUsernameUseCase.getUsername()).thenReturn(Single.fromCallable{USERNAME})

        presenter = SearchPresenterImpl(mockLogoutUseCase,
                mockGetBuddiesUseCase,
                mockCheckConnectionUseCase,
                mockUsernameUseCase)
        presenter.view = mockView
    }

    @Test
    fun testShowLogoutDialog_showLogoutDialogOnLogoutMenuOption() {
        presenter.onLogoutMenuOptionClicked()
        verify(mockView).showConfirmLogoutDialog()
    }

    @Test
    fun testLogout_logoutOnConfirmLogoutAction() {
        presenter.onLogoutConfirmed()
        verify(mockLogoutUseCase).logout()
    }

    @Test
    fun testShowResults_showOnScreenCreated() {
        reset(mockView)
        presenter.onCreated()
        verify(mockView).showProgress()
        verify(mockView).showResults(any())
        verify(mockView).hideProgress()
    }

    @Test
    fun testShowResults_showOnRefreshTriggered() {
        reset(mockView)
        presenter.onRefreshTriggered()
        verify(mockView).showProgress()
        verify(mockView).showResults(any())
        verify(mockView).hideProgress()
    }

    @Test
    fun testShowError_showErrorIfConnectionExists() {
        presenter = SearchPresenterImpl(mockLogoutUseCase,
                getErrorMockBuddiesUseCase(),
                mockCheckConnectionUseCase,
                mockUsernameUseCase)
        presenter.view = mockView

        reset(mockView)
        presenter.onRefreshTriggered()

        verify(mockView).showError()
        verify(mockView).hideProgress()
        verify(mockView, never()).showOfflineError()
        verify(mockView, never()).showResults(any())
    }

    @Test
    fun testShowOfflineError_showOfflineErrorIfNoConnectionExists() {
        `when`(mockCheckConnectionUseCase.isOnline()).thenReturn(Single.fromCallable { false })
        presenter = SearchPresenterImpl(mockLogoutUseCase,
                getErrorMockBuddiesUseCase(),
                mockCheckConnectionUseCase,
                mockUsernameUseCase)
        presenter.view = mockView

        reset(mockView)
        presenter.onRefreshTriggered()

        verify(mockView).showOfflineError()
        verify(mockView).hideProgress()
        verify(mockView, never()).showError()
        verify(mockView, never()).showResults(any())
    }

    @Test
    fun testShowProfile_showProfileOnBuddyClicked() {
        presenter.onBuddyItemClicked("id1")
        verify(mockView).showProfile("id1")
    }

    private fun  getErrorMockBuddiesUseCase(): GetBuddiesUseCase {
        return object : GetBuddiesUseCase {
            override fun getCachedBuddies(query: String): Observable<List<Buddy>> {
                return Observable.error(Exception("Test exception"))
            }

            override fun refreshBuddies(query: String): Observable<List<Buddy>> {
                return Observable.error(Exception("Test exception"))
            }

            override fun getBuddies(query: String): Observable<List<Buddy>> {
                return Observable.error(Exception("Test exception"))
            }
        }
    }

}