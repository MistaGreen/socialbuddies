package com.socialbuddies.ui.login.presenter

import android.test.mock.MockContext
import android.view.View
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.anyOrNull
import com.nhaarman.mockito_kotlin.verify
import com.socialbuddies.domain.network.usecase.CheckNetworkConnectionUseCase
import com.socialbuddies.domain.sns.usecase.MockRegisterLoginSnsCallbackUseCase
import com.socialbuddies.domain.sns.usecase.RegisterLoginSnsCallbackUseCase
import com.socialbuddies.test.ImmediateSchedulerInitializer
import com.socialbuddies.ui.login.presenter.contract.LoginView
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

/**
 * Created 10/05/17
 */
class LoginPresenterImplTest {

    lateinit var presenter: LoginPresenterImpl

    @Mock lateinit var mockView: LoginView
    @Mock lateinit var mockRegisterLoginCallback: RegisterLoginSnsCallbackUseCase
    @Mock lateinit var mockCheckNetworkConnection: CheckNetworkConnectionUseCase

    @Before
    fun initMocks() {
        ImmediateSchedulerInitializer.initImmediateScheduler()
        MockitoAnnotations.initMocks(this)

        `when`(mockView.getLoginView()).thenReturn(View(MockContext()))

        presenter = LoginPresenterImpl(mockRegisterLoginCallback, mockCheckNetworkConnection)
        presenter.view = mockView
    }

    @Test
    fun testRegisterLoginCallback_callbackShouldBeRegisteredOnCreated() {
        presenter.onCreated()
        `verify`(mockRegisterLoginCallback).registerLoginCallback(anyOrNull(), any())
    }

    @Test
    fun testOfflineError_shouldDisplayOfflineErrorIfUserIsOffline() {
        val anotherMockRegisterLoginCallback = MockRegisterLoginSnsCallbackUseCase()
        presenter = LoginPresenterImpl(anotherMockRegisterLoginCallback, mockCheckNetworkConnection)

        presenter.view = mockView
        presenter.onCreated()

        `when`(mockCheckNetworkConnection.isOnline()).thenReturn(Single.fromCallable { false })
        anotherMockRegisterLoginCallback.callError()

        verify(mockView).showOfflineError()
    }

    @Test
    fun testLoginError_shouldDisplayLoginErrorIfErrorHappened() {
        val anotherMockRegisterLoginCallback = MockRegisterLoginSnsCallbackUseCase()
        presenter = LoginPresenterImpl(anotherMockRegisterLoginCallback, mockCheckNetworkConnection)

        presenter.view = mockView
        presenter.onCreated()

        `when`(mockCheckNetworkConnection.isOnline()).thenReturn(Single.fromCallable { true })
        anotherMockRegisterLoginCallback.callError()

        verify(mockView).showLoginError()
    }

    @Test
    fun testLoginException_shouldDisplayOfflineErrorIfExceptionHappenedOnOnlineCheck() {
        val anotherMockRegisterLoginCallback = MockRegisterLoginSnsCallbackUseCase()
        presenter = LoginPresenterImpl(anotherMockRegisterLoginCallback, mockCheckNetworkConnection)

        presenter.view = mockView
        presenter.onCreated()

        `when`(mockCheckNetworkConnection.isOnline()).thenReturn(Single.fromCallable { throw Exception("Test exception") })
        anotherMockRegisterLoginCallback.callError()

        verify(mockView).showOfflineError()
    }

}