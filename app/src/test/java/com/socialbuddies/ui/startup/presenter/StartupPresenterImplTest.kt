package com.socialbuddies.ui.startup.presenter

import com.nhaarman.mockito_kotlin.verify
import com.socialbuddies.domain.sns.usecase.IsSnsLoggedInUseCase
import com.socialbuddies.test.ImmediateSchedulerInitializer
import com.socialbuddies.ui.startup.presenter.contract.StartupView
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

/**
 * Created 11/05/17
 */
class StartupPresenterImplTest {

    lateinit var presenter: StartupPresenterImpl

    @Mock lateinit var mockIsSnsLoggedInUseCase: IsSnsLoggedInUseCase
    @Mock lateinit var mockView: StartupView

    @Before
    fun initMocks() {
        ImmediateSchedulerInitializer.initImmediateScheduler()
        MockitoAnnotations.initMocks(this)

        presenter = StartupPresenterImpl(mockIsSnsLoggedInUseCase)
        presenter.view = mockView
    }

    @Test
    fun testShowLoginScreen_shouldShowLoginScreenIfSnsIsNotLoggedIn() {
        `when`(mockIsSnsLoggedInUseCase.isLoggedIn()).thenReturn(Single.fromCallable { false })
        presenter.onCreated()
        verify(mockView).showLoginScreen()
    }

    @Test
    fun testShowSearchScreen_shouldShowSearchScreenIfSnsIsLoggedIn() {
        `when`(mockIsSnsLoggedInUseCase.isLoggedIn()).thenReturn(Single.fromCallable { true })
        presenter.onCreated()
        verify(mockView).showSearchScreen()
    }

}