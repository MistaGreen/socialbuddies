package com.socialbuddies.ui.app.presenter

import com.nhaarman.mockito_kotlin.verify
import com.socialbuddies.domain.sns.usecase.InitializeSnsSdkUseCase
import com.socialbuddies.ui.app.presenter.ApplicationPresenterImpl
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

/**
 * Created 10/05/17
 */
class ApplicationPresenterImplTest {

    lateinit var presenter: ApplicationPresenterImpl

    @Mock lateinit var mockInitializeSnsSdkUseCase: InitializeSnsSdkUseCase

    @Before
    fun initMocks() {
        MockitoAnnotations.initMocks(this)
        presenter = ApplicationPresenterImpl(mockInitializeSnsSdkUseCase)
    }

    @Test
    fun testSnsSdkUseCaseInitialization_snsShouldBeInitializedOnCreateMethod() {
        presenter.onCreated()
        `verify`(mockInitializeSnsSdkUseCase).init()
    }

}