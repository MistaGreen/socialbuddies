package com.socialbuddies.domain.stub

import com.socialbuddies.domain.sns.model.Buddy
import com.socialbuddies.domain.sns.model.ProfileInfo
import com.socialbuddies.domain.sns.usecase.GetBuddiesUseCase
import com.socialbuddies.domain.sns.usecase.GetProfileInfoUseCase
import io.reactivex.Observable

/**
 * Created 11/05/17
 */
class StubBuddiesManager : GetBuddiesUseCase, GetProfileInfoUseCase {

    override fun getCachedBuddies(query: String): Observable<List<Buddy>> {
        return getBuddies(query)
    }

    override fun refreshBuddies(query: String): Observable<List<Buddy>> {
        return getBuddies(query)
    }

    override fun getBuddies(query: String): Observable<List<Buddy>> {
        return Observable.fromCallable {
            listOf(Buddy("0", "Buddy 0", "buddy0"),
                    Buddy("1", "Buddy 1", "buddy1"),
                    Buddy("2", "Buddy 2", "buddy2"))
        }
    }

    override fun getProfileInfo(profileId: String): Observable<ProfileInfo> {
        return Observable.fromCallable {
            ProfileInfo("123",
                    "Buddy 123",
                    "buddy0",
                    "https://pbs.twimg.com/profile_banners/246553304/1458592355/1500x500",
                    "https://pbs.twimg.com/profile_images/701806372279734273/7feC53q2_normal.png")
        }
    }

}