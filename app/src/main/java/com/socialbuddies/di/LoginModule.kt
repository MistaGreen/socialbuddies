package com.socialbuddies.di

import com.socialbuddies.domain.network.usecase.CheckNetworkConnectionUseCase
import com.socialbuddies.domain.sns.usecase.RegisterLoginSnsCallbackUseCase
import com.socialbuddies.ui.login.presenter.LoginPresenterImpl
import com.socialbuddies.ui.login.presenter.contract.LoginPresenter
import dagger.Module
import dagger.Provides

/**
 * Created 10/05/17
 */
@Module
class LoginModule {

    @Provides
    fun provideLoginPresenter(registerLoginSnsCallbackUseCase: RegisterLoginSnsCallbackUseCase,
                              checkNetworkConnectionUseCase: CheckNetworkConnectionUseCase): LoginPresenter
            = LoginPresenterImpl(
                registerLoginSnsCallbackUseCase,
                checkNetworkConnectionUseCase)

}