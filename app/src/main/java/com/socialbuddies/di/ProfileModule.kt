package com.socialbuddies.di

import com.socialbuddies.domain.network.usecase.CheckNetworkConnectionUseCase
import com.socialbuddies.domain.sns.usecase.GetLatestTweetsuseCase
import com.socialbuddies.domain.sns.usecase.GetProfileDetailsUseCase
import com.socialbuddies.domain.sns.usecase.GetProfileInfoUseCase
import com.socialbuddies.ui.profile.details.presenter.DetailsPresenterImpl
import com.socialbuddies.ui.profile.details.presenter.contract.DetailsPresenter
import com.socialbuddies.ui.profile.screen.presenter.ProfilePresenterImpl
import com.socialbuddies.ui.profile.screen.presenter.contract.ProfilePresenter
import dagger.Module
import dagger.Provides

/**
 * Created 12/05/17
 */
@Module
class ProfileModule {

    @Provides
    fun provideProfilePresenter(getProfileInfoUseCase: GetProfileInfoUseCase,
                                checkNetworkConnectionUseCase: CheckNetworkConnectionUseCase): ProfilePresenter
            = ProfilePresenterImpl(getProfileInfoUseCase,
                checkNetworkConnectionUseCase)

    @Provides
    fun provideDetailsPresenter(getProfileDetailsUseCase: GetProfileDetailsUseCase,
                                checkNetworkConnectionUseCase: CheckNetworkConnectionUseCase,
                                getLatestTweetsuseCase: GetLatestTweetsuseCase): DetailsPresenter
            = DetailsPresenterImpl(getProfileDetailsUseCase,
                checkNetworkConnectionUseCase,
                getLatestTweetsuseCase)

}