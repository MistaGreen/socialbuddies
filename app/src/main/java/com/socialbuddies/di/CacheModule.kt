package com.socialbuddies.di

import android.content.Context
import com.socialbuddies.domain.cache.PaperInitializeCache
import com.socialbuddies.domain.sns.twitter.cache.PaperTwitterCache
import com.socialbuddies.domain.sns.twitter.cache.TwitterCache
import com.socialbuddies.domain.sns.usecase.InitializeCacheUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created 12/05/17
 */
@Module
class CacheModule {

    @Provides
    @Singleton
    fun provideInitCacheUseCase(context: Context): InitializeCacheUseCase = PaperInitializeCache(context)

    @Provides
    @Singleton
    fun provideTwitterCache(): TwitterCache = PaperTwitterCache()

}