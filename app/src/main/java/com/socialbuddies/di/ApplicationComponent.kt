package com.socialbuddies.di

import com.socialbuddies.ui.app.view.SocialBuddiesApplication
import com.socialbuddies.ui.login.di.LoginInjector
import com.socialbuddies.ui.profile.di.ProfileInjector
import com.socialbuddies.ui.search.di.SearchInjector
import com.socialbuddies.ui.startup.di.StartupInjector
import dagger.Component
import javax.inject.Singleton

/**
 * Created 10/05/17
 */
@Singleton
@Component(modules = arrayOf(
        AndroidModule::class,
        AppModule::class,
        SnsModule::class,
        LoginModule::class,
        NetworkModule::class,
        StartupModule::class,
        SearchModule::class,
        ProfileModule::class,
        CacheModule::class))
interface ApplicationComponent :
        LoginInjector,
        StartupInjector,
        SearchInjector,
        ProfileInjector {
    fun inject(application: SocialBuddiesApplication)
}