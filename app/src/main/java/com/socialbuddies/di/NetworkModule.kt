package com.socialbuddies.di

import android.content.Context
import com.socialbuddies.domain.network.NetworkUtils
import com.socialbuddies.domain.network.usecase.CheckNetworkConnectionUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created 11/05/17
 */
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideNetworkUtils(context: Context) = NetworkUtils(context)

    @Singleton
    @Provides
    fun provideCheckNetworkConnectionUseCase(networkUtils: NetworkUtils): CheckNetworkConnectionUseCase
            = networkUtils

}