package com.socialbuddies.di

import com.socialbuddies.domain.network.usecase.CheckNetworkConnectionUseCase
import com.socialbuddies.domain.sns.usecase.GetBuddiesUseCase
import com.socialbuddies.domain.sns.usecase.GetUsernameUseCase
import com.socialbuddies.domain.sns.usecase.LogoutSnsUseCase
import com.socialbuddies.ui.search.presenter.SearchPresenterImpl
import com.socialbuddies.ui.search.presenter.contract.SearchPresenter
import dagger.Module
import dagger.Provides

/**
 * Created 11/05/17
 */
@Module
class SearchModule {

    @Provides
    fun provideSearchPresenter(logoutSnsUseCase: LogoutSnsUseCase,
                               getBuddiesUseCase: GetBuddiesUseCase,
                               checkNetworkConnectionUseCase: CheckNetworkConnectionUseCase,
                               getUsernameUseCase: GetUsernameUseCase): SearchPresenter
            = SearchPresenterImpl(logoutSnsUseCase,
            getBuddiesUseCase,
            checkNetworkConnectionUseCase,
            getUsernameUseCase)

}