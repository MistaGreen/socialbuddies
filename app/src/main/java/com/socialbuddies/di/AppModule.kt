package com.socialbuddies.di

import com.socialbuddies.domain.sns.usecase.InitializeCacheUseCase
import com.socialbuddies.domain.sns.usecase.InitializeSnsSdkUseCase
import com.socialbuddies.ui.app.presenter.ApplicationPresenterImpl
import com.socialbuddies.ui.app.presenter.contract.ApplicationPresenter
import com.socialbuddies.ui.app.usecase.GetCurrentActivityUseCase
import com.socialbuddies.ui.app.view.SocialBuddiesApplication
import com.socialbuddies.ui.app.view.navigation.Navigation
import com.socialbuddies.ui.app.view.navigation.NavigationImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created 10/05/17
 */
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideApplicationPresenter(initializeSnsSdkUseCase: InitializeSnsSdkUseCase,
                                    initializeCacheUseCase: InitializeCacheUseCase): ApplicationPresenter
            = ApplicationPresenterImpl(initializeSnsSdkUseCase, initializeCacheUseCase)

    @Provides
    @Singleton
    fun provideGetCurrentActivityUseCase(): GetCurrentActivityUseCase
            = SocialBuddiesApplication.currentActivityListener

    @Provides
    @Singleton
    fun provideNavigation(getCurrentActivityUseCase: GetCurrentActivityUseCase): Navigation
            = NavigationImpl(getCurrentActivityUseCase)

}