package com.socialbuddies.di

import com.socialbuddies.domain.sns.usecase.IsSnsLoggedInUseCase
import com.socialbuddies.ui.startup.presenter.StartupPresenterImpl
import com.socialbuddies.ui.startup.presenter.contract.StartupPresenter
import dagger.Module
import dagger.Provides

/**
 * Created 11/05/17
 */
@Module
class StartupModule {

    @Provides
    fun provideStartupPresenter(isSnsLoggedInUseCase: IsSnsLoggedInUseCase): StartupPresenter
            = StartupPresenterImpl(isSnsLoggedInUseCase)

}