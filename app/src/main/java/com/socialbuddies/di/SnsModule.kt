package com.socialbuddies.di

import android.content.Context
import com.socialbuddies.domain.sns.twitter.TwitterSdkManager
import com.socialbuddies.domain.sns.twitter.cache.TwitterCache
import com.socialbuddies.domain.sns.usecase.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created 10/05/17
 */
@Module
class SnsModule {

    @Provides
    @Singleton
    fun provideTwitterSdkManager(context: Context, twitterCache: TwitterCache)
            = TwitterSdkManager(context, twitterCache)

    @Provides
    @Singleton
    fun provideInitializeSnsSdkUseCase(twitterSdkManager: TwitterSdkManager): InitializeSnsSdkUseCase
            = twitterSdkManager

    @Provides
    @Singleton
    fun provideRegisterLoginSnsCallbackUseCase(twitterSdkManager: TwitterSdkManager): RegisterLoginSnsCallbackUseCase
            = twitterSdkManager

    @Provides
    @Singleton
    fun provideIsSnsLoggedInUseCase(twitterSdkManager: TwitterSdkManager): IsSnsLoggedInUseCase
            = twitterSdkManager

    @Provides
    @Singleton
    fun provideLogoutSnsUseCase(twitterSdkManager: TwitterSdkManager): LogoutSnsUseCase
            = twitterSdkManager

    @Provides
    @Singleton
    fun provideGetBuddiesUseCase(twitterSdkManager: TwitterSdkManager): GetBuddiesUseCase
            = twitterSdkManager

    @Provides
    @Singleton
    fun provideGetUsernameUseCase(twitterSdkManager: TwitterSdkManager): GetUsernameUseCase
            = twitterSdkManager

    @Provides
    @Singleton
    fun provideGetProfileInfoUseCase(twitterSdkManager: TwitterSdkManager): GetProfileInfoUseCase
            = twitterSdkManager

    @Provides
    @Singleton
    fun provideGetProfileDetailsUseCase(twitterSdkManager: TwitterSdkManager): GetProfileDetailsUseCase
            = twitterSdkManager

    @Provides
    @Singleton
    fun provideGetLatestTweetsUseCase(twitterSdkManager: TwitterSdkManager): GetLatestTweetsuseCase
            = twitterSdkManager

}