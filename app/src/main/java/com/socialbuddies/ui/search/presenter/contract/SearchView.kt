package com.socialbuddies.ui.search.presenter.contract

import com.socialbuddies.ui.common.presenter.contract.LoadDataView
import com.socialbuddies.ui.search.presenter.model.BuddyUiModel

/**
 * Created 11/05/17
 */
interface SearchView : LoadDataView {

    fun showConfirmLogoutDialog()
    fun showLoginScreen()
    fun showResults(results: List<BuddyUiModel>)
    fun setUsername(username: String?)
    fun showProfile(id: String)

}