package com.socialbuddies.ui.search.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.socialbuddies.R
import com.socialbuddies.ui.search.presenter.model.BuddyUiModel
import kotlinx.android.synthetic.main.search_list_item.view.*

/**
 * Created 12/05/17
 */
class BuddyItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val nameView = itemView.name
    private val loginView = itemView.login

    fun bind(buddyUiModel: BuddyUiModel, listener: OnClickListener) {
        nameView.text = buddyUiModel.name
        loginView.text = buddyUiModel.login
        itemView.setOnClickListener { listener.onClick(buddyUiModel.id) }
    }

    interface OnClickListener {
        fun onClick(id: String)
    }

    companion object {
        fun buildView(inflater: LayoutInflater, parentView: ViewGroup): View {
            return inflater.inflate(R.layout.search_list_item, parentView, false)
        }
    }

}