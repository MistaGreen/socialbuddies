package com.socialbuddies.ui.search.presenter

import com.socialbuddies.domain.network.usecase.CheckNetworkConnectionUseCase
import com.socialbuddies.domain.sns.model.Buddy
import com.socialbuddies.domain.sns.usecase.GetBuddiesUseCase
import com.socialbuddies.domain.sns.usecase.GetUsernameUseCase
import com.socialbuddies.domain.sns.usecase.LogoutSnsUseCase
import com.socialbuddies.ui.common.presenter.model.ErrorUiModel
import com.socialbuddies.ui.common.presenter.model.ProgressUiModel
import com.socialbuddies.ui.common.presenter.model.UiModel
import com.socialbuddies.ui.search.presenter.contract.SearchPresenter
import com.socialbuddies.ui.search.presenter.contract.SearchView
import com.socialbuddies.ui.search.presenter.model.BuddiesListUiModel
import com.socialbuddies.ui.search.presenter.model.BuddyUiModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created 11/05/17
 */
class SearchPresenterImpl(private val logoutSnsUseCase: LogoutSnsUseCase,
                          private val getBuddiesUseCase: GetBuddiesUseCase,
                          private val checkNetworkConnectionUseCase: CheckNetworkConnectionUseCase,
                          private val getUsernameUseCase: GetUsernameUseCase) : SearchPresenter {

    override var view: SearchView? = null

    private var query: String = ""

    override fun onLogoutMenuOptionClicked() {
        view?.showConfirmLogoutDialog()
    }

    override fun onLogoutConfirmed() {
        logoutSnsUseCase.logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess {
                    view?.showLoginScreen()
                }
                .subscribe()
    }

    override fun onCreated() {
        getBuddiesList()
        setUsername()
    }

    private fun setUsername() {
        getUsernameUseCase.getUsername()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError {
                    showError()
                }
                .doOnSuccess { username ->
                    view?.setUsername(username)
                }
                .subscribe()
    }

    override fun onRefreshTriggered() {
        refreshBuddiesList()
    }

    override fun onBuddyItemClicked(id: String) {
        view?.showProfile(id)
    }

    override fun onSearch(query: CharSequence) {
        this.query = query.toString()
        getBuddiesUseCase.getCachedBuddies(this.query)
                .let{ observable -> processBuddiesListResult(observable, false) }
    }

    private fun getBuddiesList() {
        getBuddiesUseCase.getBuddies(query)
                .let{ observable -> processBuddiesListResult(observable) }
    }

    private fun refreshBuddiesList() {
        getBuddiesUseCase.refreshBuddies(query)
                .let{ observable -> processBuddiesListResult(observable) }
    }

    private fun  processBuddiesListResult(observable: Observable<List<Buddy>>, showProgress: Boolean = true) {
        observable.subscribeOn(Schedulers.io())
                .map<UiModel> { buddies -> getBuddiesListUiModel(buddies) }
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { error -> ErrorUiModel() }
                .startWith(ProgressUiModel())
                .doOnNext { uiModel ->
                    when (uiModel) {
                        is ProgressUiModel -> if (showProgress) view?.showProgress()
                        is ErrorUiModel -> showError()
                        is BuddiesListUiModel -> showResults(uiModel.buddies)
                    }
                }
                .subscribe()
    }

    private fun showResults(results: List<BuddyUiModel>) {
        view?.hideProgress()
        view?.showResults(results)
    }

    private fun showError() {
        checkNetworkConnectionUseCase.isOnline()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { isOnline ->
                    view?.hideProgress()
                    if (isOnline) {
                        view?.showError()
                    } else {
                        view?.showOfflineError()
                    }
                }
                .subscribe()
    }

    private fun getBuddiesListUiModel(buddies: List<Buddy>): BuddiesListUiModel {
        val uiBuddies = buddies.map { buddy -> BuddyUiModel(buddy.id, buddy.name, "@${buddy.login}") }
                .sortedBy { buddy -> buddy.login.toLowerCase() }
        return BuddiesListUiModel(uiBuddies)
    }

    override fun onStart() {
    }

    override fun onStop() {
        view = null
    }

}