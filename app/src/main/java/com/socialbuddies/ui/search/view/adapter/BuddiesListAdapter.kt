package com.socialbuddies.ui.search.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.socialbuddies.ui.search.presenter.model.BuddyUiModel

/**
 * Created 12/05/17
 */
class BuddiesListAdapter(var buddies: List<BuddyUiModel>) : RecyclerView.Adapter<BuddyItemViewHolder>() {

    var clickListener: BuddyItemClickListener? = null

    override fun onBindViewHolder(holder: BuddyItemViewHolder, position: Int) {
        holder.bind(buddies[position], getOnBuddyItemClickListener())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BuddyItemViewHolder {
        val buddyView = BuddyItemViewHolder.buildView(LayoutInflater.from(parent.context), parent)
        return BuddyItemViewHolder(buddyView)
    }

    override fun getItemCount(): Int {
        return buddies.size
    }

    fun setNewBuddies(newBuddies: List<BuddyUiModel>) {
        buddies = newBuddies
        notifyDataSetChanged()
    }

    private fun getOnBuddyItemClickListener(): BuddyItemViewHolder.OnClickListener {
        return object : BuddyItemViewHolder.OnClickListener {
            override fun onClick(id: String) {
                clickListener?.onBuddyItemClicked(id)
            }
        }
    }

}