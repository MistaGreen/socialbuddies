package com.socialbuddies.ui.search.view.adapter

/**
 * Created 12/05/17 by alex
 */
interface BuddyItemClickListener {

    fun onBuddyItemClicked(id: String)

}