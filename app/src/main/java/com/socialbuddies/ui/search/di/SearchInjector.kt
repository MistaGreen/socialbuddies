package com.socialbuddies.ui.search.di

import com.socialbuddies.ui.search.view.SearchActivity

/**
 * Created 11/05/17
 */
interface SearchInjector {

    fun inject(target: SearchActivity)

}