package com.socialbuddies.ui.search.view

import android.content.Intent
import android.os.Bundle
import android.support.v4.internal.view.SupportMenuItem
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.SupportMenuInflater
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.socialbuddies.R
import com.socialbuddies.ui.app.view.navigation.Navigation
import com.socialbuddies.ui.common.view.adapter.SearchButtonAdapter
import com.socialbuddies.ui.common.view.showConfirmDialog
import com.socialbuddies.ui.common.view.showErrorSnackbar
import com.socialbuddies.ui.search.di.SearchInjector
import com.socialbuddies.ui.search.presenter.contract.SearchPresenter
import com.socialbuddies.ui.search.presenter.contract.SearchView
import com.socialbuddies.ui.search.presenter.model.BuddyUiModel
import com.socialbuddies.ui.search.view.adapter.BuddiesListAdapter
import com.socialbuddies.ui.search.view.adapter.BuddyItemClickListener
import kotlinx.android.synthetic.main.empty_view.*
import kotlinx.android.synthetic.main.search_activity.*
import javax.inject.Inject

/**
 * Created 11/05/17
 */
class SearchActivity : AppCompatActivity(), SearchView {

    @Inject
    lateinit var presenter: SearchPresenter
    @Inject
    lateinit var navigation: Navigation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as SearchInjector).inject(this)
        initUi()
        initPresenter()
    }

    private fun initPresenter() {
        presenter.view = this
        presenter.onCreated()
    }

    private fun initUi() {
        setContentView(R.layout.search_activity)
        refresh.setOnRefreshListener { presenter.onRefreshTriggered() }
        initBuddiesList()
    }

    private fun initBuddiesList() {
        buddies_list.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        buddies_list.layoutManager = LinearLayoutManager(this)
    }

    override fun onStart() {
        super.onStart()
        presenter.view = this
        presenter.onStart()
    }

    override fun onStop() {
        presenter.onStop()
        super.onStop()
    }

    override fun showConfirmLogoutDialog() {
        showConfirmDialog(R.string.search_logout_dialog_message, R.string.dialog_button_logout, R.string.dialog_button_cancel,
        { presenter.onLogoutConfirmed() })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = SupportMenuInflater(this)
        menuInflater.inflate(R.menu.menu_search, menu)
        menu?.let { initSearchButton(menu) }
        return true
    }

    private fun initSearchButton(menu: Menu) {
        val searchMenuItem = menu.findItem(R.id.action_search) as SupportMenuItem
        setSearchExpandListener(menu, searchMenuItem)
        initSearchView(searchMenuItem)
    }

    private fun initSearchView(menuItemSearch: SupportMenuItem) {
        val searchAdapter = SearchButtonAdapter(this)
        searchAdapter.init(menuItemSearch,
                resources.getString(R.string.search_hint),
                getSearchAutocompleteListener())
    }

    private fun getSearchAutocompleteListener(): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                presenter.onSearch(s)
            }

            override fun afterTextChanged(s: Editable) {

            }
        }
    }

    private fun setSearchExpandListener(menu: Menu, menuItemSearch: SupportMenuItem) {
        val menuOverflow = menu.findItem(R.id.menu_overflow)

        menuItemSearch.setSupportOnActionExpandListener(object : MenuItemCompat.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                menuOverflow.setVisible(false)
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                menuOverflow.setVisible(true)
                return true
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.logout -> presenter.onLogoutMenuOptionClicked()
        }

        return true
    }

    override fun showLoginScreen() {
        navigation.showLoginScreen(this, Navigation.REQUEST_LOGIN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Navigation.REQUEST_LOGIN) {
            finish()
        }
    }

    override fun showProgress() {
        refresh.isRefreshing = true
    }

    override fun hideProgress() {
        refresh.isRefreshing = false
    }

    override fun showResults(results: List<BuddyUiModel>) {
        var adapter = buddies_list.adapter
        if (adapter == null) {
            adapter = BuddiesListAdapter(results)
            adapter.clickListener = getBuddyItemClickListener()
            buddies_list.adapter = adapter
        } else {
            (adapter as BuddiesListAdapter).setNewBuddies(results)
        }

        if (results.size > 0) {
            empty_view.visibility = View.GONE
        } else {
            showEmptyScreen()
        }
    }

    override fun showError() {
        showErrorSnackbar(R.string.search_errorMessage)
        showEmptyScreen()
    }

    private fun showEmptyScreen() {
        if (buddies_list.childCount == 0) {
            empty_view.visibility = View.VISIBLE
        }
    }

    override fun showOfflineError() {
        showErrorSnackbar(R.string.network_offlineErrorMessage)
        showEmptyScreen()
    }

    override fun setUsername(username: String?) {
        username?.let {
            val format = resources.getString(R.string.search_title_format)
            val title = format.format(it)
            supportActionBar?.setTitle(title)
        }
    }

    override fun showProfile(id: String) {
        navigation.showProfileScreen(id)
    }

    private fun getBuddyItemClickListener(): BuddyItemClickListener {
        return object : BuddyItemClickListener {
            override fun onBuddyItemClicked(id: String) {
                presenter.onBuddyItemClicked(id)
            }
        }
    }

}