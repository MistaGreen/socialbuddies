package com.socialbuddies.ui.search.presenter.contract

import com.socialbuddies.ui.common.presenter.contract.Presenter

/**
 * Created 11/05/17
 */
interface SearchPresenter : Presenter<SearchView> {

    fun onLogoutMenuOptionClicked()
    fun onLogoutConfirmed()
    fun onRefreshTriggered()
    fun onBuddyItemClicked(id: String)
    fun onSearch(query: CharSequence)

}