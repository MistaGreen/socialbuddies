package com.socialbuddies.ui.search.presenter.model

import com.socialbuddies.ui.common.presenter.model.UiModel

/**
 * Created 11/05/17
 */
data class BuddyUiModel(val id: String,
                        val name: String,
                        val login: String) : UiModel