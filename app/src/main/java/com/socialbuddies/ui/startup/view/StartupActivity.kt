package com.socialbuddies.ui.startup.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.socialbuddies.ui.app.view.navigation.Navigation
import com.socialbuddies.ui.common.view.hideToolbar
import com.socialbuddies.ui.startup.di.StartupInjector
import com.socialbuddies.ui.startup.presenter.contract.StartupPresenter
import com.socialbuddies.ui.startup.presenter.contract.StartupView
import javax.inject.Inject

/**
 * Created 11/05/17
 */
class StartupActivity : AppCompatActivity(), StartupView {

    @Inject
    lateinit var presenter: StartupPresenter
    @Inject
    lateinit var navigation: Navigation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as StartupInjector).inject(this)

        initUi()

        presenter.view = this
        presenter.onCreated()
    }

    private fun initUi() {
        hideToolbar()
    }

    override fun showSearchScreen() {
        navigation.showSearchScreen()
    }

    override fun showLoginScreen() {
        navigation.showLoginScreen()
    }

}