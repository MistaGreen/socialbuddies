package com.socialbuddies.ui.startup.presenter

import com.socialbuddies.domain.sns.usecase.IsSnsLoggedInUseCase
import com.socialbuddies.ui.startup.presenter.contract.StartupPresenter
import com.socialbuddies.ui.startup.presenter.contract.StartupView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created 11/05/17
 */
class StartupPresenterImpl(val isSnsLoggedInUseCase: IsSnsLoggedInUseCase) : StartupPresenter {

    override var view: StartupView? = null

    override fun onCreated() {
        isSnsLoggedInUseCase.isLoggedIn()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { isLoggedIn ->
                    if (isLoggedIn) {
                        view?.showSearchScreen()
                    } else {
                        view?.showLoginScreen()
                    }
                }
                .subscribe()
    }

    override fun onStart() {
    }

    override fun onStop() {
    }
}