package com.socialbuddies.ui.startup.di

import com.socialbuddies.ui.startup.view.StartupActivity

/**
 * Created 11/05/17
 */
interface StartupInjector {

    fun inject(target: StartupActivity)

}