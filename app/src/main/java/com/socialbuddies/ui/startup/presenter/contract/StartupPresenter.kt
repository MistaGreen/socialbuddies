package com.socialbuddies.ui.startup.presenter.contract

import com.socialbuddies.ui.common.presenter.contract.Presenter

/**
 * Created 11/05/17
 */
interface StartupPresenter : Presenter<StartupView> {

}