package com.socialbuddies.ui.startup.presenter.contract

/**
 * Created 11/05/17
 */
interface StartupView {

    fun showSearchScreen()
    fun showLoginScreen()

}