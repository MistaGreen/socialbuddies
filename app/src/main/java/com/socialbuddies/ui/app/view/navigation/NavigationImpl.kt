package com.socialbuddies.ui.app.view.navigation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.socialbuddies.ui.app.usecase.GetCurrentActivityUseCase
import com.socialbuddies.ui.login.view.LoginActivity
import com.socialbuddies.ui.profile.screen.view.ProfileActivity
import com.socialbuddies.ui.search.view.SearchActivity

/**
 * Created 11/05/17
 */
class NavigationImpl(private val getCurrentActivityUseCase: GetCurrentActivityUseCase) : Navigation {

    override fun showLoginScreen(activity: Activity?, requestCode: Int) {
        showScreen(activity, LoginActivity::class.java, requestCode)
    }

    override fun showSearchScreen(activity: Activity?, requestCode: Int) {
        showScreen(activity, SearchActivity::class.java, requestCode)
    }

    override fun showProfileScreen(id: String, activity: Activity?, requestCode: Int) {
        val arguments = Bundle()
        arguments.putString(ProfileActivity.PROFILE_ID, id)
        showScreen(activity, ProfileActivity::class.java, requestCode, arguments)
    }

    private fun <T : Activity> showScreen(activity: Activity?,
                                          screen: Class<T>,
                                          requestCode: Int,
                                          arguments: Bundle? = null) {
        val currentActivity = activity ?: getCurrentActivityUseCase.getCurrentActivity()
        currentActivity?.let { activity ->
            val intent = Intent(activity, screen)
            arguments?.let { intent.putExtras(arguments) }
            when (requestCode) {
                Navigation.REQUEST_NONE -> activity.startActivity(intent)
                else -> activity.startActivityForResult(intent, requestCode)
            }
        }
    }

}