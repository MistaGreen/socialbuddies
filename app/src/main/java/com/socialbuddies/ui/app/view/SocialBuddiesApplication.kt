package com.socialbuddies.ui.app.view

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex
import com.socialbuddies.di.AndroidModule
import com.socialbuddies.di.ApplicationComponent
import com.socialbuddies.di.DaggerApplicationComponent
import com.socialbuddies.ui.app.presenter.contract.ApplicationPresenter
import com.socialbuddies.ui.app.presenter.contract.ApplicationView
import com.socialbuddies.ui.login.di.LoginInjector
import com.socialbuddies.ui.login.view.LoginActivity
import com.socialbuddies.ui.profile.di.ProfileInjector
import com.socialbuddies.ui.profile.details.view.DetailsFragment
import com.socialbuddies.ui.profile.screen.view.ProfileActivity
import com.socialbuddies.ui.search.di.SearchInjector
import com.socialbuddies.ui.search.view.SearchActivity
import com.socialbuddies.ui.startup.di.StartupInjector
import com.socialbuddies.ui.startup.view.StartupActivity
import javax.inject.Inject

/**
 * Created 10/05/17
 */
class SocialBuddiesApplication : Application(),
        ApplicationView,
        LoginInjector,
        StartupInjector,
        SearchInjector,
        ProfileInjector {

    @Inject
    lateinit var presenter: ApplicationPresenter

    override fun onCreate() {
        super.onCreate()
        setupDependencyInjection()
        registerActivityLifecycleCallbacks(currentActivityListener)

        presenter.view = this
        presenter.onCreated()
    }

    override fun inject(target: LoginActivity) {
        graph.inject(target)
    }

    override fun inject(target: StartupActivity) {
        graph.inject(target)
    }

    override fun inject(target: SearchActivity) {
        graph.inject(target)
    }

    override fun inject(target: ProfileActivity) {
        graph.inject(target)
    }

    override fun inject(target: DetailsFragment) {
        graph.inject(target)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    private fun setupDependencyInjection() {
        graph = DaggerApplicationComponent.builder()
                .androidModule(AndroidModule(this))
                .build()
        graph.inject(this)
    }

    companion object {
        @JvmStatic
        lateinit var graph: ApplicationComponent

        val currentActivityListener = CurrentActivityListener()
    }

}