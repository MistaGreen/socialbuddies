package com.socialbuddies.ui.app.view

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.socialbuddies.ui.app.usecase.GetCurrentActivityUseCase

/**
 * Created 11/05/17
 */
class CurrentActivityListener : Application.ActivityLifecycleCallbacks, GetCurrentActivityUseCase {

    private var currentActivity: Activity? = null

    override fun getCurrentActivity(): Activity? {
        return currentActivity
    }

    override fun onActivityStarted(activity: Activity?) {
        currentActivity = activity
    }

    override fun onActivityStopped(p0: Activity?) {
    }

    override fun onActivityPaused(p0: Activity?) {
    }

    override fun onActivityResumed(p0: Activity?) {
    }

    override fun onActivityDestroyed(p0: Activity?) {
    }

    override fun onActivitySaveInstanceState(p0: Activity?, p1: Bundle?) {
    }

    override fun onActivityCreated(p0: Activity?, p1: Bundle?) {
    }

}