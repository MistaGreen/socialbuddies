package com.socialbuddies.ui.app.usecase

import android.app.Activity

/**
 * Created 11/05/17
 */
interface GetCurrentActivityUseCase {

    fun getCurrentActivity(): Activity?

}