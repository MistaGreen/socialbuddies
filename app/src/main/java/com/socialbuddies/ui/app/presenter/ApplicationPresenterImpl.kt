package com.socialbuddies.ui.app.presenter

import com.socialbuddies.domain.sns.usecase.InitializeCacheUseCase
import com.socialbuddies.domain.sns.usecase.InitializeSnsSdkUseCase
import com.socialbuddies.ui.app.presenter.contract.ApplicationPresenter
import com.socialbuddies.ui.app.presenter.contract.ApplicationView

/**
 * Created 10/05/17
 */
class ApplicationPresenterImpl(private val initializeSnsSdkUseCase: InitializeSnsSdkUseCase,
                               private val initializeCacheUseCase: InitializeCacheUseCase)
    : ApplicationPresenter {

    override var view: ApplicationView? = null

    override fun onCreated() {
        initializeSnsSdkUseCase.init()
        initializeCacheUseCase.init()
    }

    override fun onStart() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onStop() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}