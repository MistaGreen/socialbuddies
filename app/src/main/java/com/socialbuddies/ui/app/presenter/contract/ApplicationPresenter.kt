package com.socialbuddies.ui.app.presenter.contract

import com.socialbuddies.ui.common.presenter.contract.Presenter

/**
 * Created 10/05/17
 */
interface ApplicationPresenter : Presenter<ApplicationView> {
}