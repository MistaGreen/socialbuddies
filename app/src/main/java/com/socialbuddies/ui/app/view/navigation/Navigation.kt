package com.socialbuddies.ui.app.view.navigation

import android.app.Activity

/**
 * Created 11/05/17
 */
interface Navigation {

    fun showSearchScreen(activity: Activity? = null, requestCode: Int = REQUEST_NONE)
    fun showLoginScreen(activity: Activity? = null, requestCode: Int = REQUEST_NONE)
    fun showProfileScreen(id: String, activity: Activity? = null, requestCode: Int = REQUEST_NONE)

    companion object {
        val REQUEST_NONE = 100
        val REQUEST_SEARCH = 101
        val REQUEST_LOGIN = 102
    }

}