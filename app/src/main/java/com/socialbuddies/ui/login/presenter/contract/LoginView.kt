package com.socialbuddies.ui.login.presenter.contract

import android.view.View

/**
 * Created 10/05/17
 */
interface LoginView {

    fun getLoginView(): View
    fun showSearchScreen()
    fun showLoginError()
    fun showOfflineError()

}