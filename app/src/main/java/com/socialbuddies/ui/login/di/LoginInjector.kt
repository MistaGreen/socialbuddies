package com.socialbuddies.ui.login.di

import com.socialbuddies.ui.login.view.LoginActivity

/**
 * Created 10/05/17
 */
interface LoginInjector {

    fun inject(target: LoginActivity)

}