package com.socialbuddies.ui.login.presenter

import com.socialbuddies.domain.network.usecase.CheckNetworkConnectionUseCase
import com.socialbuddies.domain.sns.usecase.RegisterLoginSnsCallbackUseCase
import com.socialbuddies.domain.sns.usecase.RegisterLoginSnsCallbackUseCase.LoginCallback
import com.socialbuddies.ui.login.presenter.contract.LoginPresenter
import com.socialbuddies.ui.login.presenter.contract.LoginView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created 10/05/17
 */
class LoginPresenterImpl(private val registerLoginSnsCallbackUseCase: RegisterLoginSnsCallbackUseCase,
                         private val checkNetworkConnectionUseCase: CheckNetworkConnectionUseCase) : LoginPresenter {

    override var view: LoginView? = null

    private var loginCallback: LoginCallback = lazy { buildLoginCallback() }.value

    override fun onCreated() {
        view?.let {
            registerLoginSnsCallbackUseCase.registerLoginCallback(it.getLoginView(), loginCallback)
        }
    }

    override fun onStart() {
    }

    override fun onStop() {
    }

    private fun buildLoginCallback(): LoginCallback {
        return object : LoginCallback {
            override fun onSuccess() {
                view?.showSearchScreen()
            }

            override fun onError() {
                showError()
            }
        }
    }

    private fun showError() {
        checkNetworkConnectionUseCase.isOnline()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { error ->
                    false
                }
                .doOnSuccess { isOnline ->
                    if (isOnline) {
                        view?.showLoginError()
                    } else {
                        view?.showOfflineError()
                    }
                }
                .subscribe()
    }

    companion object {
        private val TAG = "LoginPresenterImpl"
    }

}