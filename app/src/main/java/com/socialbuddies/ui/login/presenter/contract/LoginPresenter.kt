package com.socialbuddies.ui.login.presenter.contract

import com.socialbuddies.ui.common.presenter.contract.Presenter

/**
 * Created 10/05/17
 */
interface LoginPresenter : Presenter<LoginView> {
}