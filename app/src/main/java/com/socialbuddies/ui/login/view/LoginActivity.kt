package com.socialbuddies.ui.login.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.socialbuddies.R
import com.socialbuddies.ui.app.view.navigation.Navigation
import com.socialbuddies.ui.common.view.hideToolbar
import com.socialbuddies.ui.common.view.showErrorSnackbar
import com.socialbuddies.ui.login.di.LoginInjector
import com.socialbuddies.ui.login.presenter.contract.LoginPresenter
import com.socialbuddies.ui.login.presenter.contract.LoginView
import kotlinx.android.synthetic.main.login_activity.*
import javax.inject.Inject

/**
 * Created 10/05/17
 */
class LoginActivity : AppCompatActivity(), LoginView {

    @Inject
    lateinit var presenter: LoginPresenter
    @Inject
    lateinit var navigation: Navigation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as LoginInjector).inject(this)

        setContentView(R.layout.login_activity)
        initUi()

        presenter.view = this
        presenter.onCreated()
    }

    private fun initUi() {
        hideToolbar()
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun onStop() {
        presenter.onStop()
        super.onStop()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        login_button.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Navigation.REQUEST_SEARCH) {
            finish()
        }
    }

    override fun getLoginView(): View {
        return login_button
    }

    override fun showSearchScreen() {
        navigation.showSearchScreen(this, Navigation.REQUEST_SEARCH)
    }

    override fun showLoginError() {
        showErrorSnackbar(R.string.login_errorMessage)
    }

    override fun showOfflineError() {
        showErrorSnackbar(R.string.network_offlineErrorMessage)
    }

}