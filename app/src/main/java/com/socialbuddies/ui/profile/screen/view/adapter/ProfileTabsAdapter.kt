package com.socialbuddies.ui.profile.screen.view.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.socialbuddies.R
import com.socialbuddies.ui.profile.details.view.DetailsFragment

/**
 * Created 12/05/17
 */
class ProfileTabsAdapter(private val profileId: String,
                         fragmentManager: FragmentManager,
                         context: Context) : FragmentStatePagerAdapter(fragmentManager) {

    private val TABS_COUNT = 2

    val POSITION_DETAILS = 0
    val POSITION_ACTIVITIES = 1

    private var titles: Array<String>

    init {
        titles = buildTitles(context)
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            POSITION_DETAILS -> DetailsFragment(profileId)
            POSITION_ACTIVITIES -> Fragment()
            else -> throw IndexOutOfBoundsException("Invalid tab index")
        }
    }

    override fun getCount(): Int {
        return TABS_COUNT
    }

    override fun getPageTitle(position: Int): CharSequence {
        return titles[position]
    }

    private fun buildTitles(context: Context): Array<String> {
        val titles = ArrayList<String>(TABS_COUNT)
        titles.add(POSITION_DETAILS, context.resources.getString(R.string.profile_tab_details))
        titles.add(POSITION_ACTIVITIES, context.resources.getString(R.string.profile_tab_activities))
        return titles.toTypedArray()
    }

}