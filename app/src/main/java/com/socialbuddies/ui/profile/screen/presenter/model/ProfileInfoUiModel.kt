package com.socialbuddies.ui.profile.screen.presenter.model

import com.socialbuddies.domain.sns.model.ProfileInfo
import com.socialbuddies.ui.common.presenter.model.UiModel

/**
 * Created 12/05/17
 */
data class ProfileInfoUiModel(val profileInfo: ProfileInfo) : UiModel