package com.socialbuddies.ui.profile.details.presenter.model

import com.socialbuddies.domain.sns.model.ProfileDetails
import com.socialbuddies.ui.common.presenter.model.UiModel

/**
 * Created 12/05/17
 */
data class ProfileDetailsUiModel(val details: ProfileDetails) : UiModel