package com.socialbuddies.ui.profile.screen.presenter

import com.socialbuddies.domain.network.usecase.CheckNetworkConnectionUseCase
import com.socialbuddies.domain.sns.model.ProfileInfo
import com.socialbuddies.domain.sns.usecase.GetProfileInfoUseCase
import com.socialbuddies.ui.common.presenter.model.ErrorUiModel
import com.socialbuddies.ui.common.presenter.model.ProgressUiModel
import com.socialbuddies.ui.common.presenter.model.UiModel
import com.socialbuddies.ui.profile.screen.presenter.contract.ProfilePresenter
import com.socialbuddies.ui.profile.screen.presenter.contract.ProfileView
import com.socialbuddies.ui.profile.screen.presenter.model.ProfileInfoUiModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created 12/05/17
 */
class ProfilePresenterImpl(private val getProfileInfoUseCase: GetProfileInfoUseCase,
                           private val checkNetworkConnectionUseCase: CheckNetworkConnectionUseCase) : ProfilePresenter {

    override var view: ProfileView? = null

    override fun onCreated() {

    }

    override fun onCreated(profileId: String) {
        getProfileInfoUseCase.getProfileInfo(profileId)
                .subscribeOn(Schedulers.io())
                .map { profileInfo -> getProfileInfoUiModel(profileInfo) }
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { ErrorUiModel() }
                .startWith(ProgressUiModel())
                .doOnNext { uiModel ->
                    when (uiModel) {
                        is ProgressUiModel -> view?.showProgress()
                        is ErrorUiModel -> showError()
                        is ProfileInfoUiModel -> showProfileInfo(uiModel.profileInfo)
                    }
                }
                .subscribe()
    }

    private fun showProfileInfo(info: ProfileInfo) {
        view?.hideProgress()
        view?.setToolbarTitle(info.name)
        view?.setTopBackground(info.background)
        view?.setAvathar(info.avathar)
        view?.setLoginName(info.login)
    }


    private fun showError() {
        checkNetworkConnectionUseCase.isOnline()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { isOnline ->
                    view?.hideProgress()
                    if (isOnline) {
                        view?.showError()
                    } else {
                        view?.showOfflineError()
                    }
                }
                .subscribe()
    }

    private fun getProfileInfoUiModel(profileInfo: ProfileInfo): UiModel {
        return ProfileInfoUiModel(profileInfo)
    }

    override fun onStart() {
    }

    override fun onStop() {
    }

}