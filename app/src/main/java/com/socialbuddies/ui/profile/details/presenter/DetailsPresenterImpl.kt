package com.socialbuddies.ui.profile.details.presenter

import com.socialbuddies.domain.network.usecase.CheckNetworkConnectionUseCase
import com.socialbuddies.domain.sns.model.ProfileDetails
import com.socialbuddies.domain.sns.model.Tweet
import com.socialbuddies.domain.sns.usecase.GetLatestTweetsuseCase
import com.socialbuddies.domain.sns.usecase.GetProfileDetailsUseCase
import com.socialbuddies.ui.common.presenter.model.ErrorUiModel
import com.socialbuddies.ui.common.presenter.model.ProgressUiModel
import com.socialbuddies.ui.common.presenter.model.UiModel
import com.socialbuddies.ui.profile.details.presenter.contract.DetailsPresenter
import com.socialbuddies.ui.profile.details.presenter.contract.DetailsView
import com.socialbuddies.ui.profile.details.presenter.model.LatestTweetsUiModel
import com.socialbuddies.ui.profile.details.presenter.model.ProfileDetailsUiModel
import com.socialbuddies.ui.profile.details.presenter.model.TweetUiModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created 12/05/17
 */
class DetailsPresenterImpl(private val getProfileDetailsUseCase: GetProfileDetailsUseCase,
                           private val checkNetworkConnectionUseCase: CheckNetworkConnectionUseCase,
                           private val getLatestTweetsUseCase: GetLatestTweetsuseCase) : DetailsPresenter {

    private val LATEST_TWEETS_COUNT = 5

    override var view: DetailsView? = null

    override fun onCreated() {
    }

    override fun onStart() {
    }

    override fun onStop() {
        view = null
    }

    override fun onCreated(profileId: String) {
        refreshProfileDetails(profileId)
        refreshLatestTweets(profileId)
    }

    override fun onRefreshLatestTweets(profileId: String) {
        refreshLatestTweets(profileId)
    }

    private fun refreshLatestTweets(profileId: String) {
        getLatestTweetsUseCase.getLatestTweets(profileId, LATEST_TWEETS_COUNT)
                .subscribeOn(Schedulers.io())
                .map<UiModel> { tweets -> LatestTweetsUiModel(tweets) }
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { error -> ErrorUiModel() }
                .startWith(ProgressUiModel())
                .doOnNext { uiModel ->
                    when (uiModel) {
                        is ProgressUiModel -> view?.showLatestTweetsProgress()
                        is ErrorUiModel -> showLatestTweetsError()
                        is LatestTweetsUiModel -> showLatestTweets(uiModel.tweets)
                    }
                }
                .subscribe()
    }

    private fun showLatestTweets(tweets: List<Tweet>) {
        view?.hideLatestTweetsProgress()
        view?.showLatestTweets(tweets.map { tweet -> TweetUiModel(tweet.text) })
    }

    private fun showLatestTweetsError() {
        view?.hideLatestTweetsProgress()
        showError()
    }

    private fun refreshProfileDetails(profileId: String) {
        getProfileDetailsUseCase.getProfileDetails(profileId)
                .subscribeOn(Schedulers.io())
                .map<UiModel> { details -> ProfileDetailsUiModel(details) }
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn { ErrorUiModel() }
                .startWith(ProgressUiModel())
                .doOnNext { uiModel ->
                    when (uiModel) {
                        is ProgressUiModel -> view?.showProgress()
                        is ErrorUiModel -> showError()
                        is ProfileDetailsUiModel -> showProfileDetails(uiModel.details)
                    }
                }
                .subscribe()
    }

    private fun showProfileDetails(details: ProfileDetails) {
        view?.hideProgress()
        view?.showTweets(details.tweets.toString())
        view?.showFavourites(details.favourites.toString())
        view?.showFriends(details.friends.toString())
        view?.showFollowers(details.followers.toString())
    }

    private fun showError() {
        checkNetworkConnectionUseCase.isOnline()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { isOnline ->
                    view?.hideProgress()
                    if (isOnline) {
                        view?.showError()
                    } else {
                        view?.showOfflineError()
                    }
                }
                .subscribe()
    }

}