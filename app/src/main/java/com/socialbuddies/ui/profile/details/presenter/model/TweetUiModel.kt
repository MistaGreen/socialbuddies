package com.socialbuddies.ui.profile.details.presenter.model

/**
 * Created 12/05/17
 */
data class TweetUiModel(val text: String)