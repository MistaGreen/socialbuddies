package com.socialbuddies.ui.profile.screen.presenter.contract

import com.socialbuddies.ui.common.presenter.contract.LoadDataView

/**
 * Created 12/05/17
 */
interface ProfileView : LoadDataView {

    fun setToolbarTitle(title: String)
    fun setTopBackground(background: String)
    fun setAvathar(avathar: String)
    fun setLoginName(login: String)

}