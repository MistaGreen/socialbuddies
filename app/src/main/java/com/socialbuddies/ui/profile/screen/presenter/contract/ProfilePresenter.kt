package com.socialbuddies.ui.profile.screen.presenter.contract

import com.socialbuddies.ui.common.presenter.contract.Presenter

/**
 * Created 12/05/17
 */
interface ProfilePresenter : Presenter<ProfileView> {
    fun onCreated(profileId: String)
}