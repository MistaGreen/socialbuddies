package com.socialbuddies.ui.profile.details.presenter.model

import com.socialbuddies.domain.sns.model.Tweet
import com.socialbuddies.ui.common.presenter.model.UiModel

/**
 * Created 12/05/17
 */
data class LatestTweetsUiModel(val tweets: List<Tweet>) : UiModel