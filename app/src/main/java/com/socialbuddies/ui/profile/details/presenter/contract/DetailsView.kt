package com.socialbuddies.ui.profile.details.presenter.contract

import com.socialbuddies.ui.common.presenter.contract.LoadDataView
import com.socialbuddies.ui.profile.details.presenter.model.TweetUiModel

/**
 * Created 12/05/17
 */
interface DetailsView : LoadDataView {
    fun showTweets(tweetsString: String)
    fun showFavourites(favouritesString: String)
    fun showFriends(friendsString: String)
    fun showFollowers(followersString: String)
    fun showLatestTweetsProgress()
    fun hideLatestTweetsProgress()
    fun showLatestTweets(tweets: List<TweetUiModel>)
}