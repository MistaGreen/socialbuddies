package com.socialbuddies.ui.profile.details.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.socialbuddies.R
import com.socialbuddies.ui.common.view.showErrorSnackbar
import com.socialbuddies.ui.profile.details.presenter.contract.DetailsPresenter
import com.socialbuddies.ui.profile.details.presenter.contract.DetailsView
import com.socialbuddies.ui.profile.details.presenter.model.TweetUiModel
import com.socialbuddies.ui.profile.details.view.adapter.LatestTweetsAdapter
import com.socialbuddies.ui.profile.di.ProfileInjector
import kotlinx.android.synthetic.main.empty_view.*
import kotlinx.android.synthetic.main.profile_details_fragment.*
import javax.inject.Inject

/**
 * Created 12/05/17
 */
class DetailsFragment() : Fragment(), DetailsView {

    private val PROFILE_ID = "profile_id"

    @Inject
    lateinit var presenter: DetailsPresenter

    constructor(profileId: String): this() {
        val arguments = Bundle()
        arguments.putString(PROFILE_ID, profileId)
        setArguments(arguments)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity.application as ProfileInjector).inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.profile_details_fragment, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        initLatestTweetsList()
        presenter.view = this
        presenter.onCreated(getProfileId())
    }

    private fun initLatestTweetsList() {
        latest_tweets.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        latest_tweets.layoutManager = LinearLayoutManager(activity)
        latestTweets_progress.setOnRefreshListener { presenter.onRefreshLatestTweets(getProfileId()) }
    }

    override fun onStart() {
        super.onStart()
        presenter.view = this
        presenter.onStart()
    }

    override fun onStop() {
        presenter.onStop()
        super.onStop()
    }

    override fun showTweets(tweetsString: String) {
        tweets.text = tweetsString
    }

    override fun showFavourites(favouritesString: String) {
        favourites.text = favouritesString
    }

    override fun showFriends(friendsString: String) {
        friends.text = friendsString
    }

    override fun showFollowers(followersString: String) {
        followers.text = followersString
    }

    override fun showProgress() {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideProgress() {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showError() {
        showErrorSnackbar(R.string.profile_details_errorMessage)
        showEmptyScreen()

    }

    override fun showOfflineError() {
        showErrorSnackbar(R.string.network_offlineErrorMessage)
        showEmptyScreen()
    }

    private fun showEmptyScreen() {
        if (latest_tweets.childCount == 0) {
            empty_view.visibility = View.VISIBLE
        }
    }

    override fun showLatestTweetsProgress() {
        latestTweets_progress.isRefreshing = true
    }

    override fun hideLatestTweetsProgress() {
        latestTweets_progress.isRefreshing = false
    }

    override fun showLatestTweets(tweets: List<TweetUiModel>) {
        var adapter = latest_tweets.adapter
        if (adapter == null) {
            adapter = LatestTweetsAdapter(tweets)
            latest_tweets.adapter = adapter
        } else {
            (adapter as LatestTweetsAdapter).setNewTweets(tweets)
        }

        if (tweets.size > 0) {
            empty_view.visibility = View.GONE
        } else {
            showEmptyScreen()
        }
    }

    private fun getProfileId(): String {
        return arguments.getString(PROFILE_ID)
    }

}