package com.socialbuddies.ui.profile.details.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.socialbuddies.ui.profile.details.presenter.model.TweetUiModel
import com.socialbuddies.ui.search.view.adapter.TweetViewHolder

/**
 * Created 12/05/17
 */
class LatestTweetsAdapter(var tweets: List<TweetUiModel>) : RecyclerView.Adapter<TweetViewHolder>() {

    override fun onBindViewHolder(holder: TweetViewHolder, position: Int) {
        holder.bind(tweets[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TweetViewHolder {
        val tweetView = TweetViewHolder.buildView(LayoutInflater.from(parent.context), parent)
        return TweetViewHolder(tweetView)
    }

    override fun getItemCount(): Int {
        return tweets.size
    }

    fun setNewTweets(newTweets: List<TweetUiModel>) {
        tweets = newTweets
        notifyDataSetChanged()
    }

}