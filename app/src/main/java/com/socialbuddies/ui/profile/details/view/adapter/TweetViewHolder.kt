package com.socialbuddies.ui.search.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.socialbuddies.R
import com.socialbuddies.ui.profile.details.presenter.model.TweetUiModel
import kotlinx.android.synthetic.main.profile_details_latest_tweets_list_item.view.*

/**
 * Created 12/05/17
 */
class TweetViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val textView = itemView.text

    fun bind(tweetUiModel: TweetUiModel) {
        textView.text = tweetUiModel.text
    }

    companion object {
        fun buildView(inflater: LayoutInflater, parentView: ViewGroup): View {
            return inflater.inflate(R.layout.profile_details_latest_tweets_list_item, parentView, false)
        }
    }

}