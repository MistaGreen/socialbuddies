package com.socialbuddies.ui.profile.di

import com.socialbuddies.ui.profile.details.view.DetailsFragment
import com.socialbuddies.ui.profile.screen.view.ProfileActivity

/**
 * Created 12/05/17
 */
interface ProfileInjector {

    fun inject(target: ProfileActivity)
    fun inject(target: DetailsFragment)

}