package com.socialbuddies.ui.profile.screen.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.socialbuddies.R
import com.socialbuddies.ui.common.view.showErrorSnackbar
import com.socialbuddies.ui.profile.di.ProfileInjector
import com.socialbuddies.ui.profile.screen.presenter.contract.ProfilePresenter
import com.socialbuddies.ui.profile.screen.presenter.contract.ProfileView
import com.socialbuddies.ui.profile.screen.view.adapter.ProfileTabsAdapter
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.profile_activity.*
import javax.inject.Inject

/**
 * Created 12/05/17 by alex
 */
class ProfileActivity : AppCompatActivity(), ProfileView {

    @Inject
    lateinit var presenter: ProfilePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as ProfileInjector).inject(this)

        initUi()
        initPresenter()
    }

    private fun initPresenter() {
        presenter.view = this
        presenter.onCreated(getProfileId())
    }

    private fun getProfileId(): String {
        return intent.getStringExtra(PROFILE_ID)
    }

    private fun initUi() {
        setContentView(R.layout.profile_activity)
        initToolbar()
        initTabs()
    }

    private fun initTabs() {
        tabs_content.adapter = ProfileTabsAdapter(getProfileId(), supportFragmentManager, this)
        tabs.setupWithViewPager(tabs_content)
    }

    private fun initToolbar() {
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun showProgress() {
        profile_progress.show()
    }

    override fun hideProgress() {
        profile_progress.hide()
    }

    override fun showError() {
        showErrorSnackbar(R.string.profile_errorMessage)
    }

    override fun showOfflineError() {
        showErrorSnackbar(R.string.network_offlineErrorMessage)
    }

    override fun setToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun setTopBackground(background: String) {
        Picasso.with(this).load(background)
                .memoryPolicy(MemoryPolicy.NO_STORE)
                .into(background_image)
    }

    override fun setAvathar(avathar: String) {
        Picasso.with(this).load(avathar).into(avathar_image)
    }

    override fun setLoginName(login: String) {
        login_label.text = "@$login"
    }

    companion object {
        val PROFILE_ID = "profile id"
    }

}