package com.socialbuddies.ui.profile.details.presenter.contract

import com.socialbuddies.ui.common.presenter.contract.Presenter

/**
 * Created 12/05/17
 */
interface DetailsPresenter : Presenter<DetailsView> {

    fun onCreated(profileId: String)
    fun onRefreshLatestTweets(profileId: String)

}