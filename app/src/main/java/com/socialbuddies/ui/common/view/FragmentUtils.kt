package com.socialbuddies.ui.common.view

import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import com.socialbuddies.R

/**
 * Created 12/05/17
 */
fun Fragment.showErrorSnackbar(errorMessage: Int, duration: Int = Snackbar.LENGTH_LONG) {
    view?.let { view ->
        val mainLayout = view?.findViewById(R.id.main_layout)
        val snackbar = Snackbar.make(mainLayout, errorMessage, duration)
        snackbar.show()
    }
}