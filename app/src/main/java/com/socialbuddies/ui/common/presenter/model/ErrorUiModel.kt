package com.socialbuddies.ui.common.presenter.model

/**
 * Created 11/05/17
 */
data class ErrorUiModel(val code: Int = 0,
                        val message: String = "") : UiModel