package com.socialbuddies.ui.common.view

import android.app.Activity
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.socialbuddies.R

/**
 * Created 11/05/17
 */
fun AppCompatActivity.hideToolbar() {
    val toolbar = supportActionBar
    toolbar?.hide()
}

fun Activity.showErrorSnackbar(errorMessage: Int, duration: Int = Snackbar.LENGTH_LONG) {
    val mainLayout = findViewById(R.id.main_layout)
    val snackbar = Snackbar.make(mainLayout, errorMessage, duration)
    snackbar.show()
}

fun Activity.showConfirmDialog(message: Int, okButton: Int, cancelButton: Int, confirmAction: () -> Unit) {
    AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton(okButton, { dialogInterface, i ->
                confirmAction.invoke()
            })
            .setNegativeButton(cancelButton, null)
            .show()
}