package com.socialbuddies.ui.common.presenter.contract

/**
 * Created 10/05/17
 */
interface Presenter<T> {

    var view: T?

    fun onCreated()
    fun onStart()
    fun onStop()

}