package com.socialbuddies.ui.common.view.adapter

import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v4.internal.view.SupportMenuItem
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.SearchView
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.socialbuddies.R

/**
 * Created 12/05/17
 */
class SearchButtonAdapter(private val activity: Activity) {
    private val TAG = "PM:SearchButtonAdapter"
    val CURSOR_DRAWABLE_RES = "mCursorDrawableRes"
    val EDITOR = "mEditor"
    val CURSOR_DRAWABLE = "mCursorDrawable"

    fun init(searchMenuItem: SupportMenuItem, hint: String, autocompleteListener: TextWatcher) {
        val searchManager = activity.getSystemService(Context.SEARCH_SERVICE) as SearchManager

        val searchView = MenuItemCompat.getActionView(searchMenuItem) as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(activity.componentName))

        initSearchAutocomplete(searchView, hint, autocompleteListener)
        initSearchPlate(searchView)
        initSearchCloseIcon(searchView)
    }

    private fun initSearchPlate(searchView: SearchView) {
        val searchPlate = searchView.findViewById(R.id.search_plate)
        searchPlate.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimary))
    }

    private fun initSearchCloseIcon(searchView: SearchView) {
        val searchCloseIcon = searchView.findViewById(R.id.search_close_btn) as ImageView
        searchCloseIcon.setImageResource(R.drawable.ic_close)
        searchCloseIcon.visibility = View.VISIBLE
    }

    private fun initSearchAutocomplete(searchView: SearchView, hint: String, autocompleteListener: TextWatcher) {
        val searchAutoComplete = searchView.findViewById(R.id.search_src_text) as SearchView.SearchAutoComplete
        searchAutoComplete.hint = hint
        searchAutoComplete.setTextColor(Color.WHITE)

        searchAutoComplete.isCursorVisible = true
        setCursorDrawableColor(searchAutoComplete, ContextCompat.getColor(searchAutoComplete.context, android.R.color.white))

        searchAutoComplete.addTextChangedListener(autocompleteListener)
    }

    private fun setCursorDrawableColor(editText: SearchView.SearchAutoComplete, color: Int) {
        try {
            val mCursorDrawableRes = getCursorDrawableRes(editText)
            val editor = getEditor(editText)
            val clazz = editor.javaClass

            val fCursorDrawable = clazz.getDeclaredField(CURSOR_DRAWABLE)
            fCursorDrawable.isAccessible = true
            fCursorDrawable.set(editor, getDrawables(editText.context, color, mCursorDrawableRes))
        } catch (ignored: Throwable) {
            Log.e(TAG, ignored.toString())
        }

    }

    @Throws(NoSuchFieldException::class, IllegalAccessException::class)
    private fun getEditor(editText: SearchView.SearchAutoComplete): Any {
        val fEditor = TextView::class.java.getDeclaredField(EDITOR)
        fEditor.isAccessible = true
        return fEditor.get(editText)
    }

    @Throws(NoSuchFieldException::class, IllegalAccessException::class)
    private fun getCursorDrawableRes(editText: SearchView.SearchAutoComplete): Int {
        val fCursorDrawableRes = TextView::class.java.getDeclaredField(CURSOR_DRAWABLE_RES)
        fCursorDrawableRes.isAccessible = true
        return fCursorDrawableRes.getInt(editText)
    }

    private fun getDrawables(context: Context, color: Int, mCursorDrawableRes: Int): Array<Drawable?> {
        val drawables = arrayOfNulls<Drawable>(2)
        drawables[0] = ContextCompat.getDrawable(context, mCursorDrawableRes)
        drawables[1] = ContextCompat.getDrawable(context, mCursorDrawableRes)
        drawables[0]?.setColorFilter(color, PorterDuff.Mode.SRC_IN)
        drawables[1]?.setColorFilter(color, PorterDuff.Mode.SRC_IN)
        return drawables
    }
}