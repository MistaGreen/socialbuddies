package com.socialbuddies.ui.common.presenter.contract

/**
 * Created 12/05/17
 */
interface LoadDataView {

    fun showProgress()
    fun hideProgress()
    fun showError()
    fun showOfflineError()

}