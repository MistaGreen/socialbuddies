package com.socialbuddies.domain.sns.model

/**
 * Created 12/05/17
 */
data class ProfileDetails(val tweets: Int,
                          val favourites: Int,
                          val friends: Int,
                          val followers: Int)