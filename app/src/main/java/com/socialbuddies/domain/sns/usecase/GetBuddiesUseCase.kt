package com.socialbuddies.domain.sns.usecase

import com.socialbuddies.domain.sns.model.Buddy
import io.reactivex.Observable

/**
 * Created 11/05/17
 */
interface GetBuddiesUseCase {

    fun getBuddies(query: String): Observable<List<Buddy>>
    fun refreshBuddies(query: String): Observable<List<Buddy>>
    fun getCachedBuddies(query: String): Observable<List<Buddy>>

}