package com.socialbuddies.domain.sns.usecase

import com.socialbuddies.domain.sns.model.ProfileDetails
import io.reactivex.Observable

/**
 * Created 12/05/17
 */
interface GetProfileDetailsUseCase {

    fun getProfileDetails(profileId: String): Observable<ProfileDetails>

}