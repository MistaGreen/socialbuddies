package com.socialbuddies.domain.sns.twitter.api

import com.socialbuddies.domain.sns.twitter.api.json.TweetJson
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created 12/05/17
 */
interface TweetsService {

    @GET("/1.1/statuses/user_timeline.json")
    fun latestTweets(@Query("user_id") id: Long,
                @Query("count") count: Int) : Call<List<TweetJson>>

}