package com.socialbuddies.domain.sns.usecase

import io.reactivex.Single

/**
 * Created 12/05/17
 */
interface GetUsernameUseCase {

    fun getUsername(): Single<String>

}