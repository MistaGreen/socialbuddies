package com.socialbuddies.domain.sns.twitter.api.json

import com.google.gson.annotations.SerializedName

/**
 * Created 12/05/17
 */
class FriendsListJson {

    @SerializedName("previous_cursor")
    var previousPageId: Long = 0

    @SerializedName("next_cursor")
    var nextPageId: Long = 0

    @SerializedName("users")
    var friends: List<FriendJson> = ArrayList<FriendJson>()

}