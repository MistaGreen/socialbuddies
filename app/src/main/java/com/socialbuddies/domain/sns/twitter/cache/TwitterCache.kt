package com.socialbuddies.domain.sns.twitter.cache

import com.socialbuddies.domain.sns.twitter.api.json.FriendJson

/**
 * Created 12/05/17
 */
interface TwitterCache {

    fun putFriendsList(newFriends: List<FriendJson>)
    fun getFriend(friendId: String): FriendJson?
    fun getAllFriends(): List<FriendJson>
    fun clear()

}