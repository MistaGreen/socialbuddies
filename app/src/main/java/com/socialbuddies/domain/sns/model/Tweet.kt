package com.socialbuddies.domain.sns.model

/**
 * Created 12/05/17
 */
data class Tweet(val text: String)