package com.socialbuddies.domain.sns.usecase

/**
 * Created 10/05/17
 */
interface InitializeSnsSdkUseCase {

    fun init()

}