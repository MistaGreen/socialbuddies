package com.socialbuddies.domain.sns.twitter.cache

import com.socialbuddies.domain.sns.twitter.api.json.FriendJson
import io.paperdb.Paper

/**
 * Created 12/05/17
 */
class PaperTwitterCache : TwitterCache {

    private val BOOK = "friends"

    private val KEY_FRIEND_PREFIX = "friend_"
    private val KEY_ALL_FRIENDS = "all_friends"

    override fun putFriendsList(newFriends: List<FriendJson>) {
        newFriends.map { friend -> putFriend(friend) }
        Paper.book(BOOK).write(KEY_ALL_FRIENDS, newFriends)
    }

    private fun  putFriend(friend: FriendJson) {
        Paper.book(BOOK).write(getFriendKey(friend.id), friend)
    }

    override fun getFriend(friendId: String): FriendJson? {
        return Paper.book(BOOK).read(getFriendKey(friendId))
    }

    override fun getAllFriends(): List<FriendJson> {
        return Paper.book(BOOK).read(KEY_ALL_FRIENDS) ?: ArrayList<FriendJson>()
    }

    override fun clear() {
        Paper.book(BOOK).destroy()
    }

    private fun getFriendKey(friendId: String): String {
        return KEY_FRIEND_PREFIX + friendId
    }

}