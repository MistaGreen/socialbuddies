package com.socialbuddies.domain.sns.usecase

import android.view.View

/**
 * Created 10/05/17
 */
interface RegisterLoginSnsCallbackUseCase {

    fun registerLoginCallback(loginView: View, loginCallback: LoginCallback)

    interface LoginCallback {
        fun onSuccess()
        fun onError()
    }

}