package com.socialbuddies.domain.sns.usecase

import com.socialbuddies.domain.sns.model.Tweet
import io.reactivex.Observable

/**
 * Created 12/05/17
 */
interface GetLatestTweetsuseCase {

    fun getLatestTweets(profileId: String, numberOfTweets: Int): Observable<List<Tweet>>

}