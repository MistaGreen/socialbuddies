package com.socialbuddies.domain.sns.twitter.api.json

import com.google.gson.annotations.SerializedName

/**
 * Created 12/05/17
 */
class TweetJson {

    @SerializedName("text")
    var text: String = ""

}