package com.socialbuddies.domain.sns.twitter.api

import com.twitter.sdk.android.core.TwitterApiClient
import com.twitter.sdk.android.core.TwitterSession

/**
 * Created 12/05/17
 */
class SocialBuddiesTwitterApiClient(session: TwitterSession) : TwitterApiClient(session) {

    fun getFriendsService(): FriendsService {
        return getService(FriendsService::class.java)
    }

    fun getTweetsService(): TweetsService {
        return getService(TweetsService::class.java)
    }

}