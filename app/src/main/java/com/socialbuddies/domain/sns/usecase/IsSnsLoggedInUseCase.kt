package com.socialbuddies.domain.sns.usecase

import io.reactivex.Single

/**
 * Created 11/05/17
 */
interface IsSnsLoggedInUseCase {

    fun isLoggedIn(): Single<Boolean>

}