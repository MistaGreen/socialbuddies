package com.socialbuddies.domain.sns.model

/**
 * Created 11/05/17
 */
data class Buddy(val id: String,
                 val name: String,
                 val login: String)