package com.socialbuddies.domain.sns.twitter.api.json

import com.google.gson.annotations.SerializedName

/**
 * Created 12/05/17
 */
class FriendJson {

    @SerializedName("id_str")
    var id: String = ""

    @SerializedName("name")
    var name: String = ""

    @SerializedName("screen_name")
    var login: String = ""

    @SerializedName("profile_background_image_url")
    var background: String = ""

    @SerializedName("profile_image_url")
    var avathar: String = ""

    @SerializedName("statuses_count")
    var statusesCount: Int = 0

    @SerializedName("followers_count")
    var followersCount: Int = 0

    @SerializedName("friends_count")
    var friendsCount: Int = 0

    @SerializedName("favourites_count")
    var favouritesCount: Int = 0

}