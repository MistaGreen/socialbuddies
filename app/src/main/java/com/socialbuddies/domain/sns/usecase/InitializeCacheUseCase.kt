package com.socialbuddies.domain.sns.usecase

/**
 * Created 12/05/17
 */
interface InitializeCacheUseCase {

    fun init()

}