package com.socialbuddies.domain.sns.model

/**
 * Created 12/05/17
 */
data class ProfileInfo(val id: String,
                       val name: String,
                       val login: String,
                       val background: String,
                       val avathar: String)