package com.socialbuddies.domain.sns.usecase

import com.socialbuddies.domain.sns.model.ProfileInfo
import io.reactivex.Observable

/**
 * Created 12/05/17
 */
interface GetProfileInfoUseCase {

    fun getProfileInfo(profileId: String): Observable<ProfileInfo>

}