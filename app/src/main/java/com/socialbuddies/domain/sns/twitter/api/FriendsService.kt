package com.socialbuddies.domain.sns.twitter.api

import com.socialbuddies.domain.sns.twitter.api.json.FriendsListJson
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created 12/05/17
 */
interface FriendsService {

    @GET("/1.1/friends/list.json")
    fun friends(@Query("user_id") id: Long,
                @Query("screen_name") screenName: String,
                @Query("cursor") pageId: Long,
                @Query("count") pageSize: Int,
                @Query("skip_status") skipStatus: Boolean,
                @Query("include_user_entities") includeUserEntities: Boolean): Call<FriendsListJson>

}