package com.socialbuddies.domain.sns.twitter

import android.content.Context
import android.view.View
import com.socialbuddies.domain.sns.model.Buddy
import com.socialbuddies.domain.sns.model.ProfileDetails
import com.socialbuddies.domain.sns.model.ProfileInfo
import com.socialbuddies.domain.sns.model.Tweet
import com.socialbuddies.domain.sns.twitter.api.SocialBuddiesTwitterApiClient
import com.socialbuddies.domain.sns.twitter.api.json.FriendJson
import com.socialbuddies.domain.sns.twitter.cache.TwitterCache
import com.socialbuddies.domain.sns.usecase.*
import com.twitter.sdk.android.Twitter
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterLoginButton
import io.fabric.sdk.android.Fabric
import io.reactivex.Observable
import io.reactivex.Single


/**
 * Created 10/05/17
 */
class TwitterSdkManager(private val context: Context,
                        private val twitterCache: TwitterCache) :
        InitializeSnsSdkUseCase,
        RegisterLoginSnsCallbackUseCase,
        IsSnsLoggedInUseCase,
        LogoutSnsUseCase,
        GetBuddiesUseCase,
        GetUsernameUseCase,
        GetProfileInfoUseCase,
        GetProfileDetailsUseCase,
        GetLatestTweetsuseCase {

    override fun getUsername(): Single<String> {
        return Single.fromCallable {
            val session = getActiveSession()
            session.userName
        }
    }

    override fun init() {
        val authConfig = TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET)
        Fabric.with(context, Twitter(authConfig))
    }

    override fun registerLoginCallback(loginView: View, loginCallback: RegisterLoginSnsCallbackUseCase.LoginCallback) {
        val twitterLoginButton = loginView as TwitterLoginButton
        twitterLoginButton.callback = object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>?) {
                loginCallback.onSuccess()
            }

            override fun failure(exception: TwitterException?) {
                loginCallback.onError()
            }
        }
    }

    override fun isLoggedIn(): Single<Boolean> {
        return Single.fromCallable {
            getActiveSession() != null
        }
    }

    override fun getProfileInfo(profileId: String): Observable<ProfileInfo> {
        return Observable.fromCallable {
            val friend = twitterCache.getFriend(profileId)
            buildProfileInfo(friend) ?: error("No profile info found")
        }
    }

    override fun getProfileDetails(profileId: String): Observable<ProfileDetails> {
        return Observable.fromCallable {
            val friend = twitterCache.getFriend(profileId)
            buildProfileDetails(friend) ?: error("No profile details found")
        }
    }

    private fun buildProfileDetails(friend: FriendJson?): ProfileDetails? {
        return friend?.let { ProfileDetails(friend.statusesCount,
                friend.favouritesCount,
                friend.friendsCount,
                friend.followersCount) }
    }

    private fun buildProfileInfo(friend: FriendJson?): ProfileInfo? {
        return friend?.let { ProfileInfo(friend.id,
                friend.name,
                friend.login,
                friend.background,
                friend.avathar) }
    }

    private fun getActiveSession() = TwitterCore.getInstance().sessionManager.activeSession

    override fun logout(): Single<Any> {
        return Single.fromCallable {
            TwitterCore.getInstance().logOut()
            twitterCache.clear()
        }
    }

    override fun getBuddies(query: String): Observable<List<Buddy>> {
        val networkData = getNetworkFreinds(query)
        val cachedData = getCachedFriends(query)
        return Observable.concat(cachedData, networkData)
    }

    override fun refreshBuddies(query: String): Observable<List<Buddy>> {
        return getNetworkFreinds(query)
    }

    override fun getCachedBuddies(query: String): Observable<List<Buddy>> {
        return getCachedFriends(query)
    }

    private fun getCachedFriends(query: String): Observable<List<Buddy>> {
        return Observable.fromCallable {
            twitterCache.getAllFriends()
                    .filter { friend -> isFriendSatisfyQuery(friend, query)}
                    .map { friend -> Buddy(friend.id, friend.name, friend.login) }
        }
    }

    private fun getNetworkFreinds(query: String): Observable<List<Buddy>> {
        return Observable.fromCallable {
            val session = getActiveSession()
            val client = SocialBuddiesTwitterApiClient(session)

            val callback = client.getFriendsService().friends(session.userId,
                    session.userName,
                    -1,
                    200,
                    true,
                    false)

            val response = callback.execute()
            if (response.isSuccessful) {
                var buddies: List<Buddy> = ArrayList<Buddy>()
                response?.let { result ->
                    twitterCache.putFriendsList(result.body().friends)
                    buddies = result.body().friends
                            .filter { friend -> isFriendSatisfyQuery(friend, query) }
                            .map { friend -> Buddy(friend.id, friend.name, friend.login) }
                }
                buddies
            } else {
                error(response.message())
            }
        }
    }

    override fun getLatestTweets(profileId: String, numberOfTweets: Int): Observable<List<Tweet>> {
        return Observable.fromCallable {
            val session = getActiveSession()
            val client = SocialBuddiesTwitterApiClient(session)

            val callback = client.getTweetsService().latestTweets(profileId.toLong(), numberOfTweets)

            val response = callback.execute()
            if (response.isSuccessful) {
                var tweets: List<Tweet> = ArrayList<Tweet>()
                response?.let { result ->
                    tweets = result.body()
                            .map { tweet -> Tweet(tweet.text) }
                }
                tweets
            } else {
                error(response.message())
            }
        }
    }

    private fun isFriendSatisfyQuery(friend: FriendJson, query: String) = friend.name.contains(query, true) || friend.login.contains(query, true)

    companion object {
        private val TWITTER_KEY = "UdevMTWfZFqlibLbaYYtMxy0K"
        private val TWITTER_SECRET = "nLOi59aWjcxVFzvTdJBg2zMEUopzW9M3LHeKzuocmy5OEWnrxh"
    }

}