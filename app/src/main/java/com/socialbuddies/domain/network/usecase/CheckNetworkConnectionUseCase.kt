package com.socialbuddies.domain.network.usecase

import io.reactivex.Single

/**
 * Created 11/05/17
 */
interface CheckNetworkConnectionUseCase {

    fun isOnline(): Single<Boolean>

}