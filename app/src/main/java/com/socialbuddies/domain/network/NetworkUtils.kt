package com.socialbuddies.domain.network

import android.content.Context
import android.net.ConnectivityManager
import com.socialbuddies.domain.network.usecase.CheckNetworkConnectionUseCase
import io.reactivex.Single

/**
 * Created 11/05/17
 */
class NetworkUtils(private val context: Context) : CheckNetworkConnectionUseCase {

    override fun isOnline(): Single<Boolean> {
        return Single.fromCallable {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo
            networkInfo != null && networkInfo.isAvailable && networkInfo.isConnected
        }
    }

    companion object {
        val TAG = "NetworkUtils"
    }

}