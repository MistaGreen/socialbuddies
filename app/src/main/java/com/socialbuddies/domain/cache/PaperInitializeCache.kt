package com.socialbuddies.domain.cache

import android.content.Context
import com.socialbuddies.domain.sns.usecase.InitializeCacheUseCase
import io.paperdb.Paper

/**
 * Created 12/05/17
 */
class PaperInitializeCache(private val context: Context) : InitializeCacheUseCase {

    override fun init() {
        Paper.init(context)
    }

}